* Super simple UDP proxy that listen on a local UDP socket and relays all UDP packets to an remote host. All packets that the remote host sends back to UDP proxy will also be relayed the to local UDP client. Currently, this script works with Python 2 and 3

### How do I get set up? ###

You can make it directly executable with the command

```
#!shell

chmod +x udpproxy.py
```
and now, just run it by giving the 5 needed parameters


```
#!shell
./udpproxy.py localhost:localport:remotehost:remoteport:isVerbose

```

For example

```
#!shell
./udpproxy.py 192.168.1.10:7777:173.194.40.69:80:false

```

The UDP proxy wait for UDP packets on the socket 192.168.1.10:7777 and forward the received packets to 173.194.40.69:80
The last parameter must be set to False if you don't need verbose output. Set it to true if you want more information about what is append :-)



