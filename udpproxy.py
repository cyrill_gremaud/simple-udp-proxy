#!/usr/bin/env python
#
# @Author : Cyrill Gremaud
# @GitLink: https://cyrill_gremaud@bitbucket.org/cyrill_gremaud/simple-udp-proxy.git
#
# Super simple script that listens to a local UDP ip/port and relays all packets to an arbitrary remote host.
# Packets that the host sends back will also be relayed to the local UDP client.
# Works with Python 2 and 3

import sys, socket

def fail(reason):
	sys.stderr.write(reason + '\n')
	sys.exit(1)

if len(sys.argv) != 2 or len(sys.argv[1].split(':')) != 5:
	fail('Usage: udp-relay.py localhost:localPort:remoteHost:remotePort:isVerbose')

localHost, localPort, remoteHost, remotePort, verbose = sys.argv[1].split(':')

if verbose != "False" and verbose != "false":
	verbose = "True"

try:
	localPort = int(localPort)
except:
	fail('Invalid port number: ' + str(localPort))
try:
	remotePort = int(remotePort)
except:
	fail('Invalid port number: ' + str(remotePort))

try:
	s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
	s.bind((localHost, localPort))
except:
	fail('Failed to bind ' + str(localHost) + '  on port ' + str(localPort) + '\n')
	fail('Please be sure that ' + str(localHost) + ' exists and that ' + str(localPort) + ' is not already used')

knownClient = None
knownServer = (remoteHost, remotePort)
sys.stderr.write('Waiting UDP on ' + str(localHost) + ':' + str(localPort) + ' for incoming packets\n')

if verbose == "True":
	sys.stderr.write('Print [rx] for each received UDP packets\n')
	sys.stderr.write('Print [txs] for each forwarded packets to a known server\n')
	sys.stderr.write('Print [txc] for each forwarded packets to a known client\n')

while True:
	data, addr = s.recvfrom(32768)
	if verbose == "True":
		sys.stderr.write('[rx]')
	if knownClient is None:
		knownClient = addr 
	if addr == knownClient:
		s.sendto(data, knownServer)
		if verbose == "True":
			sys.stderr.write('[txs]')
	else:
		s.sendto(data, knownClient)
		if verbose == "Verbose":
			sys.stderr.write('[txc]')
